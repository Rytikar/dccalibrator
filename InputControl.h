#pragma once

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

const char CONNECTREQUEST = 'C';
const char VOLTSREQUEST = 'V';
const char OUTPUTREQUEST = 'O';
const char STOREREQUEST = 'S';
const char FIRMWAREREQUEST = 'F';
const char MEMORYREQUEST = 'M';
const char TRANSIENTREQUEST = 'T';
const char STATUSRESPONSE = 'S';
const char MEMORYRESPONSE = 'M';
const char FIRMWARERESPONSE = 'F';

struct RequestArguments
{
	char Request;
	bool Redraw;
	RequestArguments(char request, bool redraw)
	{
		Request = request;
		Redraw = redraw;
	}
};

struct VoltsRequestArguments : public RequestArguments
{
	float Volts;
	VoltsRequestArguments(char request, bool redraw, float volts) : RequestArguments(request, redraw)
	{
		Volts = volts;
	}
};

struct OutputRequestArguments : public RequestArguments
{
	bool ToOn;
	OutputRequestArguments(char request, bool redraw, bool toOn) : RequestArguments(request, redraw)
	{
		ToOn = toOn;
	}
};

typedef enum TransientMode { TransientOff, TransientToggle, TransientLoop, TransientRaise, TransientFall };

struct TransientRequestArguments : public RequestArguments
{
	float LowVolts;
	float HighVolts;
	float StepVolts;
	int MilliSeconds;
	TransientMode Mode;
	TransientRequestArguments(char request, bool redraw, float lowVolts, float highVolts, float stepVolts, int milliSeconds, TransientMode mode) : RequestArguments(request, redraw)
	{
		LowVolts = lowVolts;
		HighVolts = highVolts;
		StepVolts = stepVolts;
		MilliSeconds = milliSeconds;
		Mode = mode;
	}
};

typedef void(*handleRequestCallback)(RequestArguments* requestArguments);

class InputControl
{
 public:
	 virtual void checkInput(handleRequestCallback handleRequest) = 0;
};

