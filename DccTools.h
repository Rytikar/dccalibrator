#pragma once

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include <SPI.h>
#include <TFT_ILI9341.h>
#include <MCP4922.h>
#include "DccConst.h"
#include "InputControl.h"

typedef enum Font { Font1 = 1, Font2 = 2, Font4 = 4 };
typedef enum DrawPosition { TextCenter, TextLeft, TextRight };
const Font KEYPADFONT = Font4;
const char ARGUMENTDELIMITER = ',';

class DccTools
{
private:
	static void setDac(MCP4922* dac, byte a4, byte a1, byte a3, int volts);
protected:
public:
	static const char* inputToA(float input, int _inputFracLength = INPUTFRACLENGTH, int width = INPUTMAXLENGTH);
	static bool isIn(int value, int minValue, int maxValue);
	static bool isIn(int x, int y, int minX, int maxX, int minY, int maxY);
	static float readFloat(int address);
	static float writeFloat(int address, float value);
	static int readInt(int address);
	static int writeInt(int address, int value);
	static void printFrame(TFT_ILI9341* tft, const char *title = "Voltage Reference");
	static void drawBox(TFT_ILI9341* tft, int x, int y, int w, int h, uint16_t lineColor, uint16_t backgroundColor);
	static void drawCrossHair(TFT_ILI9341* tft, int x, int y, uint16_t color);
	static const char* drawString(TFT_ILI9341* tft, const char* s, int x, int y, Font font, uint16_t fgColor, uint16_t bgColor, uint8_t textSize = 1, DrawPosition position = TextCenter);
	static char* floatToStr(float value, int width, int precision, char* result, char filler = SPACECHAR);
	static float str2Float(const char * str);
	static float getVolts();
	static float setVolts(MCP4922* dac, float volts);
	static bool isTransient();
	static TransientMode getTransientMode();
	static void setTransientMode(TransientMode transientMode);
	static bool getOutputState();
	static bool setOutputState(bool isOn);
	static float getFloatUserValue(int idx);
	static float setFloatUserValue(int idx, float value);
	static int getIntUserValue(int idx);
	static int setIntUserValue(int idx, int value);
	static char* parseArgument(const char* src, char* dst, int* idx);
};

