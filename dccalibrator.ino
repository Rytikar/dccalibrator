//SCULLCOM HOBBY ELECTRONICS
//DC VOLTAGE CALIBRATOR
//Using TFT Display with Touch Screen
//with both 1.024V and 4.096V references

#include <TFT_ILI9341.h>		//https://github.com/Bodmer/TFT_ILI9341
#include <URTouch.h>            //http://www.rinkydinkelectronics.com/download.php?f=URTouch.zip
#include <MCP4922.h>            //https://github.com/helgenodland/MCP4922-Arduino-SPI-Library/archive/master.zip
#include <SPI.h>
#include "DccConst.h"
#include "DccTools.h"
#include "SerialInputControl.h"
#include "TouchInputControl.h"
#include "InputControl.h"

#if !defined(LOAD_FONT2) || !defined(LOAD_FONT4)
#error *** Define LOAD_FONT2 and LOAD_FONT4 in User_Setup.h of TFT_ILI9341 library.
#endif

#if TFT_CS != 10 || TFT_DC != 9 || TFT_RST != 8
#error *** Define TFT_CS to 10, TFT_DC to 9 and TFT_RST to 8 in User_Setup.h of TFT_ILI9341 library.
#endif

#if !defined(FAST_LINE)
#error *** Define FAST_LINE in User_Setup.h of TFT_ILI9341 library.
#endif

#if !defined(SUPPORT_TRANSACTIONS)
#error *** Define SUPPORT_TRANSACTIONS in User_Setup.h of TFT_ILI9341 library.
#endif

//------------------------------------Global variables------------------------------------------------------------------

TFT_ILI9341 tft = TFT_ILI9341();
URTouch ts(TOUCH_SCK, TOUCH_CS, TOUCH_MOSI, TOUCH_MISO, TOUCH_IRQ); // Create an object of the sensor module and inform the library of the pinout for working with it
MCP4922 dac(DAC_MOSI, DAC_SCK, DAC_CS, DAC_LDAC);					// (MOSI,SCK,CS,LDAC) define Connections for UNO_board,

TouchInputControl* _touchInputControl = NULL;
SerialInputControl* _serialInputControl = NULL;

bool _isRemote = false;

float _transientLowVolts;
float _transientHighVolts;
float _transientStepVolts;
int _transientMilliSeconds;
unsigned long _transientNextMillis;
bool _transientLoopUp;

//---------------------------------------------------------------------------------------------------------------

void printIntro()
{
	tft.fillScreen(TFT_BLACK);
	DccTools::printFrame(&tft, "Calibrate");
	DccTools::drawString(&tft, "Touch to calibrate!", TFTHALFW, TFTHALFH, Font2, TFT_WHITE, TFT_BLACK);
	int m = millis() + 2000;
	bool touched = false;
	while (!touched && m > millis()) touched = ts.dataAvailable();
	if (touched) calibrate();
}

void printMainDisplay(float input)
{
	DccTools::printFrame(&tft);
	DccTools::drawString(&tft, "VOLTS", 5, VOLTSY, Font4, TFT_WHITE, TFT_BLACK, 2, TextLeft);
	DccTools::drawBox(&tft, SETVOLTX, SETVOLTY, SETVOLTW, SETVOLTH, TFT_YELLOW, TFT_WHITE);
	DccTools::drawString(&tft, "Set Volts", SETVOLTX + (SETVOLTW/2), SETVOLTY + 12, Font2, TFT_BLUE, TFT_WHITE, 1);
	DccTools::drawBox(&tft, SETTRANSIENTX, SETTRANSIENTY, SETTRANSIENTW, SETTRANSIENTH, TFT_YELLOW, TFT_WHITE);
	DccTools::drawString(&tft, "Set Transient", SETTRANSIENTX + (SETTRANSIENTW/2), SETTRANSIENTY + 12, Font2, TFT_BLUE, TFT_WHITE, 1);
	printState(input);
	printRemote(_isRemote);
}

void printState(float volts)
{
	printVoltage(DccTools::setVolts(&dac, volts));
	printOutputState(DccTools::getOutputState());
}

bool printRemote(bool isRemote)
{
	DccTools::drawString(&tft, "Remote", 2, 220, Font2, isRemote ? TFT_WHITE : TFT_BLACK, TFT_BLACK, 1, TextLeft);
	return isRemote;
}

void printVoltage(float volts)
{
	DccTools::drawBox(&tft, 160, VOLTSY, 28, 52, TFT_BLACK, TFT_BLACK);
	DccTools::drawString(&tft, DccTools::inputToA(volts), 315, VOLTSY, Font4, TFT_YELLOW, TFT_BLACK, 2, TextRight);
}

bool printOutputState(bool isOn)
{
	bool isTransient = DccTools::isTransient();
	DccTools::drawBox(&tft, TOGGLEVOLTX, TOGGLEVOLTY, TOGGLEVOLTW, TOGGLEVOLTH, TFT_YELLOW, TFT_WHITE);
	DccTools::drawString(&tft, isOn ? (isTransient ? "ON(T)" : "ON") : (isTransient ? "OFF(T)" : "OFF"), TOGGLEVOLTX + (TOGGLEVOLTW/2), TOGGLEVOLTY + 10, KEYPADFONT, isOn ? TFT_DARKGREEN : TFT_BLUE, TFT_WHITE, KEYPADTEXTSIZE);
	return isOn;
}

//--------------------------------------------------------------------------------------------------------------------

void calibrate()
{
	ts.read();
	delay(200);
	tft.fillScreen(TFT_BLACK);
	DccTools::drawString(&tft, "Touch top left crosshair!", 160, (240 / 2) - 20, Font2, TFT_WHITE, TFT_BLACK);
	DccTools::drawCrossHair(&tft, 10, 10, TFT_WHITE);

	while (!ts.dataAvailable());
	ts.read();
	int tlX = ts.getX();
	int tlY = ts.getY();
	DccTools::drawCrossHair(&tft, 10, 10, TFT_BLACK);

	delay(500);
	DccTools::drawString(&tft, "Now bottom right crosshair!", 160, (240 / 2) + 20, Font2, TFT_WHITE, TFT_BLACK);
	DccTools::drawCrossHair(&tft, 310, 230, TFT_WHITE);
	while (!ts.dataAvailable());
	ts.read();
	int brX = ts.getX();
	int brY = ts.getY();

	DccTools::writeInt(CALIBRATIONEEADDRESS, tlX > TFTHALFW ? TFTW : 0);
	DccTools::writeInt(CALIBRATIONEEADDRESS + 2, tlY > TFTHALFH ? TFTH : 0);

	DccTools::drawString(&tft, "Calibration stored!", 160, (240 / 2) + 60, Font2, TFT_WHITE, TFT_BLACK);
	delay(2000);
}

//---------------------------------------------------------------------------------------------------------

InputControl* getInputControl()
{
	if (_serialInputControl->checkRemote())
	{
		if (!_isRemote) _isRemote = printRemote(true);
		return _serialInputControl;
	}
	else
	{
		if (_isRemote) _isRemote = printRemote(false);
		return _touchInputControl;
	}
}

void handleRequest(RequestArguments* arguments)
{
	switch(arguments->Request)
	{ 
	case VOLTSREQUEST: handleVoltsRequest((VoltsRequestArguments*)arguments); break;
	case TRANSIENTREQUEST: handleTransientRequest((TransientRequestArguments*)arguments); break;
	case OUTPUTREQUEST: handleOutputRequest((OutputRequestArguments*)arguments); break;
	}
	delete arguments;
}

void handleVoltsRequest(VoltsRequestArguments* arguments)
{
	DccTools::setTransientMode(TransientOff);
	if (arguments->Redraw) printMainDisplay(DccTools::setVolts(&dac, arguments->Volts));
	else printState(arguments->Volts);
}

void handleTransientRequest(TransientRequestArguments* arguments)
{
	DccTools::setTransientMode(arguments->Mode);
	_transientLowVolts = arguments->LowVolts;
	_transientHighVolts = arguments->HighVolts;
	_transientStepVolts = arguments->StepVolts;
	_transientMilliSeconds = arguments->MilliSeconds;
	_transientLoopUp = true;

	if (_transientLowVolts > _transientHighVolts)
	{
		float tmp = _transientLowVolts;
		_transientLowVolts = _transientHighVolts;
		_transientHighVolts = tmp;
	}
	if (_transientLowVolts < 0) _transientLowVolts = 0;
	if (_transientHighVolts > MAXVOLTS) _transientLowVolts = MAXVOLTS;
	
	float volts = arguments->Mode == TransientFall ? _transientHighVolts : _transientLowVolts;

	if (arguments->Redraw) printMainDisplay(DccTools::setVolts(&dac, volts));
	else printState(volts);
}

void handleOutputRequest(OutputRequestArguments* arguments)
{
	printOutputState(DccTools::setOutputState(arguments->ToOn));
	if (DccTools::getOutputState() && DccTools::isTransient()) _transientNextMillis = millis() + _transientMilliSeconds;
}

//------------------------------Transient Routines---------------------------------------------------------

void handleTransient()
{
	const float delta = 0.000001;
		
	if (!DccTools::isTransient() || !DccTools::getOutputState()) return;

	unsigned long m = millis();
	if (m < _transientNextMillis) return;
	
	_transientNextMillis = m + _transientMilliSeconds;

	float volts = DccTools::getVolts();
	switch(DccTools::getTransientMode())
	{ 
	case TransientToggle: volts = volts == _transientLowVolts ? _transientHighVolts : _transientLowVolts; break;
	case TransientLoop: 
	{
		volts += _transientLoopUp ? _transientStepVolts : -_transientStepVolts;
		if (_transientLoopUp && (volts >= _transientHighVolts))
		{
			volts = _transientHighVolts;
			_transientLoopUp = false;
		}
		else if (!_transientLoopUp && volts <= _transientLowVolts)
		{
			volts = _transientLowVolts;
			_transientLoopUp = true;
		}
		break;
	}
	case TransientRaise: 
		{
			volts += _transientStepVolts;
			if (volts > _transientHighVolts && abs(volts - _transientHighVolts) > delta) volts = _transientLowVolts;
			break;
		}
	case TransientFall: 
		{
			volts -= _transientStepVolts;
			if (volts < _transientLowVolts && abs(volts - _transientLowVolts) > delta) volts = _transientHighVolts;
			break;
		}
	}

	printVoltage(DccTools::setVolts(&dac, volts));
}

//------------------------------Programme Set-up Routines--------------------------------------------------

void setup()
{
	Serial.begin(9600);               //start serial monitor at 9600 baud
	Serial.setTimeout(100);

	pinMode(A0, OUTPUT);              //set A0 as a digital output pin
	digitalWrite(A0, LOW);            //set A0 output LOW
	pinMode(A1, OUTPUT);              //set A1 as a digital output pin
	digitalWrite(A1, HIGH);            //set A1 output HIGH
	pinMode(A2, OUTPUT);              //set A2 as a digital output pin
	digitalWrite(A2, LOW);            //set A2 output LOW
	pinMode(A3, OUTPUT);              //set A3 as a digital output pin
	digitalWrite(A3, HIGH);            //set A3 output HIGH
	pinMode(A4, OUTPUT);              //set A4 as a digital output pin
	digitalWrite(A4, HIGH);            //set A4 output HIGH 

	tft.init();                      //Initialize the start of work with the graphic display
	tft.setRotation(1);               //we translate the display into landscape orientation

	ts.InitTouch();                   //Initialize the touchscreen display module
	ts.setPrecision(PREC_MEDIUM);     //Determine the necessary accuracy of the processing of the pressures: PREC_LOW - low, PREC_MEDIUM - medium, PREC_HI - high, PREC_EXTREME - maximum

	DccTools::setOutputState(false);
	DccTools::setVolts(&dac, 8);
	DccTools::setVolts(&dac, 0);

	printIntro();
	printMainDisplay(DccTools::getVolts());

	_touchInputControl = new TouchInputControl(&tft, &ts, DccTools::readInt(CALIBRATIONEEADDRESS + 2), DccTools::readInt(CALIBRATIONEEADDRESS));
	_serialInputControl = new SerialInputControl();
}

//------------------------------Main Programme Loop--------------------------------------------------

void loop()
{
	getInputControl()->checkInput(&handleRequest);
	handleTransient();
}

