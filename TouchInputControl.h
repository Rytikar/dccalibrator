#pragma once

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include <TFT_ILI9341.h>
#include <URTouch.h>
#include "DccConst.h"
#include "InputControl.h"

struct Coordinates
{
	int X;
	int Y;
	Coordinates(int x, int y)
	{
		X = x;
		Y = y;
	}
};

class TouchInputControl : public InputControl
{
private:
	TFT_ILI9341* _tft;
	URTouch * _ts;
	int _deltaHeight;
	int _deltaWidth;
	Coordinates _coordinates = Coordinates(0, 0);
	byte _inputLength;
	byte _inputIntLength;
	byte _inputFracLength;
	int _inputMaxValue;

	bool _inDecimals;
	int _cursor;

	bool getVoltage(float* volts);
	bool getTransient(float* startVolts, float* endVolts, float* stepVolts, int* milliSeconds, TransientMode* mode);
	bool getOutputState(bool* outputState);
	bool readTouchScreen(Coordinates* coordinates);
	TransientMode readTransientMode(const char* label);
	float readKeyPad(const char* title, const char* label, byte inputIntLength = INPUTINTLENGTH, byte inputFracLength = INPUTFRACLENGTH, int inputMaxValue = INPUTMAXVALUE);
	void printKeypad(const char* label);
	void printUserValue(int i);
	void drawGrid(int x, int y, int width, int height, byte columns, int rows);
	void printInput(uint16_t color);
	void resetKeys();
	void handleKey(byte col, byte row);
	void setCursor(int cursor);
	void toggleInDecimals();
	void addKey(char key);
	void deleteKey();
	bool flashError();
	bool setKeys(const char* keys, int cursor);
	bool touchIn(int x, int y, int minX, int maxX, int minY, int maxY);
	float keysToFloat();
	float getUserValue(int idx);
	void  setUserValue(int idx, float value);
	
public:
	TouchInputControl(TFT_ILI9341 * tft, URTouch* ts, int deltaHeight, int deltaWidth);
	void checkInput(handleRequestCallback handleRequest) override;
};

