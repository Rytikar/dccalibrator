#include "SerialInputControl.h"
#include "DccConst.h"
#include "DccTools.h"

const byte NEWLINE = 10;
const byte MAXREQUESTLENGTH = 30;
const unsigned long TIMEOUTMS = 2000;
char _command[MAXREQUESTLENGTH + 1];

SerialInputControl::SerialInputControl()
{
	resetCommand();
}

bool SerialInputControl::checkRemote()
{
	if (_isConnected && !isTimedOut()) return true;
	return _isConnected = readCommand(_command);
}

void SerialInputControl::checkInput(handleRequestCallback handleRequest)
{
	if (!readCommand(_command)) return;
	int idx = 1;
	switch(_command[0])
	{ 
	case CONNECTREQUEST: sendStatus(); break;
	case MEMORYREQUEST: sendMemory();  break;
	case FIRMWAREREQUEST: sendFirmware();  break;
	case STOREREQUEST: handleStoreRequest(); break;
	case VOLTSREQUEST: handleRequest(new VoltsRequestArguments(VOLTSREQUEST, false, parseFloat(_command, &idx)));  break;
	case TRANSIENTREQUEST: handleTransientRequest(handleRequest);  break;
	case OUTPUTREQUEST: handleRequest(new OutputRequestArguments(OUTPUTREQUEST, false, _command[1] != '0'));  break;
	}
	resetCommand();
}

void SerialInputControl::handleTransientRequest(handleRequestCallback handleRequest)
{
	int idx = 1;
	float loVolts = parseFloat(_command, &idx);
	float hiVolts = parseFloat(_command, &idx);
	float stepVolts = parseFloat(_command, &idx);
	int durationMs = parseInt(_command, &idx);
	TransientMode transientMode = (TransientMode)parseInt(_command, &idx);
	handleRequest(new TransientRequestArguments(TRANSIENTREQUEST, false, loVolts, hiVolts, stepVolts, durationMs, transientMode));
}

void SerialInputControl::handleStoreRequest()
{
	int idx = 1;
	bool intValue = parseInt(_command, &idx) != 0;
	int slot = parseInt(_command, &idx);
	float value = parseFloat(_command, &idx);
	intValue ? DccTools::setIntUserValue(slot, value) : DccTools::setFloatUserValue(slot, value);
}

float SerialInputControl::parseFloat(const char* s, int* idx)
{
	char tmp[10];
	return DccTools::str2Float(DccTools::parseArgument(s, tmp, idx));
}

int SerialInputControl::parseInt(const char* s, int* idx)
{
	char tmp[10];
	return atoi(DccTools::parseArgument(s, tmp, idx));
}

bool SerialInputControl::resetCommand()
{
	return _command[0] = NULCHAR;
}

bool SerialInputControl::pendingCommand()
{
	return _command[0] != NULCHAR;
}

void SerialInputControl::resetTimeout()
{
	_heartbeatMillis = millis() + TIMEOUTMS;
}

bool SerialInputControl::isTimedOut()
{
	return millis() > _heartbeatMillis;
}

void SerialInputControl::sendStatus()
{
	finishSend(sendBool(DccTools::getOutputState(), false, sendInt(DccTools::getTransientMode(), true, sendFloat(DccTools::getVolts(), true, sendChar(STATUSRESPONSE)))));
}

void SerialInputControl::sendMemory()
{
	byte checkSum = sendChar(MEMORYRESPONSE);
	for (int i = 0; i < MAXUSERVALUES; i++) checkSum = sendFloat(DccTools::getFloatUserValue(i), true, checkSum);
	for (int i = 0; i < MAXUSERVALUES; i++) checkSum = sendInt(DccTools::getIntUserValue(i), i < MAXUSERVALUES-1, checkSum);
	finishSend(checkSum);
}

void SerialInputControl::sendFirmware()
{
	finishSend(sendString(DCCVERSION, false, sendChar(FIRMWARERESPONSE)));
}

byte SerialInputControl::sendFloat(float value, bool sendDelimiter, byte checkSum)
{
	const char* str = DccTools::inputToA(value, INPUTFRACLENGTH, -1);
	for (int i = 0; str[i]; i++) checkSum = sendChar(str[i], checkSum);
	return sendDelimiter ? sendChar(ARGUMENTDELIMITER, checkSum) : checkSum;
}

byte SerialInputControl::sendInt(int value, bool sendDelimiter, byte checkSum)
{
	const char* str = DccTools::inputToA(value, 0, -1);
	for (int i = 0; str[i]; i++) checkSum = sendChar(str[i], checkSum);
	return sendDelimiter ? sendChar(ARGUMENTDELIMITER, checkSum) : checkSum;
}

byte SerialInputControl::sendBool(bool value, bool sendDelimiter, byte checkSum)
{
	checkSum = sendChar(value ? '1' : '0', checkSum);
	return sendDelimiter ? sendChar(ARGUMENTDELIMITER, checkSum) : checkSum;
}

byte SerialInputControl::sendString(const char* s, bool sendDelimiter, byte checkSum)
{
	while (*s)
	{
		checkSum = sendChar(*s, checkSum); 
		s++;
	}
	return sendDelimiter ? sendChar(ARGUMENTDELIMITER, checkSum) : checkSum;
}

byte SerialInputControl::sendChar(char c, byte checksum = 0)
{
	Serial.write((byte)c);
	return checksum + (byte)c;
}

void SerialInputControl::finishSend(byte checkSum)
{
	if (checkSum > 128) checkSum -= 128;
	if (checkSum < 32) checkSum += 32;
	Serial.write(checkSum);
	Serial.write(NEWLINE);
}

bool SerialInputControl::readCommand(char* command)
{
	if (pendingCommand()) return true;

	int commandRead = 0;
	int i;
	while (commandRead < MAXREQUESTLENGTH && (i = serialRead()) >= 0 && i != NEWLINE) command[commandRead++] = (char)i;

	if (commandRead < 2) return resetCommand();

	byte cs = command[commandRead-1];
	command[commandRead-1] = NULCHAR;
	if (checksum(command) != cs) return resetCommand();

	resetTimeout();
	return true;
}

byte SerialInputControl::checksum(const char* command)
{
	byte checkSum = 0;
	for (int i = 0; command[i]; i++) checkSum += command[i];
	if (checkSum > 128) checkSum -= 128;
	if (checkSum < 32) checkSum += 32;
	return checkSum;
}

int SerialInputControl::serialRead()
{
	if (!Serial.available()) return -1;
	int i = Serial.read();
	delay(10);
	return i;
}