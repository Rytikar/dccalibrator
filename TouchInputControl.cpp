#include "TouchInputControl.h"
#include "DccConst.h"
#include "DccTools.h"

const char KEYPADKEYS[KEYPADROWS][KEYPADCOLUMNS] = { { '1','2','3' },{ '4','5','6' },{ '7','8','9' },{ DECIMALPOINTCHAR,'0', DELETECHAR } };
const char* modes[] = { "Off", "Toggle", "Loop", "Raise", "Fall" };
char _keys[INPUTMAXLENGTH +1];

TouchInputControl::TouchInputControl(TFT_ILI9341 * tft, URTouch* ts, int deltaHeight, int deltaWidth)
{
	_tft = tft;
	_ts = ts;
	_deltaHeight = deltaHeight;
	_deltaWidth = deltaWidth;
}

void TouchInputControl::checkInput(handleRequestCallback handleRequest)
{
	float volts1 = 0;
	float volts2 = 0;
	float volts3 = 0;
	int int1 = 0;
	TransientMode mode;
	bool outputState = DccTools::getOutputState();
	if (getVoltage(&volts1)) handleRequest(new VoltsRequestArguments(VOLTSREQUEST, true, volts1));
	if (getTransient(&volts1, &volts2, &volts3, &int1, &mode)) handleRequest(new TransientRequestArguments(TRANSIENTREQUEST, true, volts1, volts2, volts3, int1, mode));
	if (getOutputState(&outputState)) handleRequest(new OutputRequestArguments(OUTPUTREQUEST, false, outputState));
}

bool TouchInputControl::touchIn(int x, int y, int minX, int maxX, int minY, int maxY)
{
	return readTouchScreen(&_coordinates) && DccTools::isIn(x, y, minX, maxX, minY, maxY);
}

bool TouchInputControl::getVoltage(float* volts)
{
	if (!touchIn(_coordinates.X, _coordinates.Y, SETVOLTX, SETVOLTX + SETVOLTW, SETVOLTY, SETVOLTY + SETVOLTH))  return false;
	*volts = readKeyPad("Set Volts", "Volt");
	delay(INPUTDELAY);
	return true;
}

bool TouchInputControl::getTransient(float* lowVolts, float* highVolts, float* stepVolts, int* milliSeconds, TransientMode* mode)
{
	char title[21] = "Set Transient ";
	char* s;

	if (!touchIn(_coordinates.X, _coordinates.Y, SETTRANSIENTX, SETTRANSIENTX + SETTRANSIENTW, SETTRANSIENTY, SETTRANSIENTY + SETTRANSIENTH))  return false;
	*mode = readTransientMode("Mode");
	s = strcat(title, modes[*mode] );

	*lowVolts = readKeyPad(s, "Low Volt");
	*highVolts = readKeyPad(s, "High Volt");
	*stepVolts = *mode == TransientToggle ? 0 : readKeyPad(s, "Step Volt");
	*milliSeconds = readKeyPad(s, "mS", 4, 0, 9999);
	delay(INPUTDELAY);
	return true;
}

bool TouchInputControl::getOutputState(bool* outputState)
{
	if (!touchIn(_coordinates.X, _coordinates.Y, TOGGLEVOLTX, TOGGLEVOLTX + TOGGLEVOLTW, TOGGLEVOLTY, TOGGLEVOLTY + TOGGLEVOLTH)) return false;
	*outputState = !*outputState;
	delay(INPUTDELAY);
	return true;
}

bool TouchInputControl::readTouchScreen(Coordinates* coordinates)
{
	if (!_ts->dataAvailable()) return false;

	_ts->read();
	coordinates->X = _deltaWidth ? _deltaWidth - _ts->getX() : _ts->getX();
	coordinates->Y = _deltaHeight ? _deltaHeight - _ts->getY() : _ts->getY();
	return true;
}

void TouchInputControl::drawGrid(int x, int y, int width, int height, byte columns, int rows)
{
	_tft->fillRect(x, y, columns*width, rows*height, TFT_RED);

	for (int i = 0; i <= columns; i++) _tft->drawFastVLine(x + (i*width), y, (rows*height), TFT_WHITE);
	for (int i = 0; i <= rows; i++) _tft->drawFastHLine(x, y + (i*height), (columns*width), TFT_WHITE);
}

void TouchInputControl::printUserValue(int i)
{
	DccTools::drawBox(_tft, KEYPADFUNCTIONX, KEYPADFUNCTIONY + (i*KEYPADHEIGHT), KEYPADFUNCTIONWIDTH, KEYPADHEIGHT, TFT_WHITE, TFT_RED);
	DccTools::drawString(_tft, DccTools::inputToA(getUserValue(i), _inputFracLength), KEYPADFUNCTIONX + KEYPADFUNCTIONWIDTH - 5 , KEYPADFUNCTIONY + (i*KEYPADHEIGHT) + KEYPADDELTAY, KEYPADFONT, TFT_YELLOW, TFT_RED, KEYPADTEXTSIZE, TextRight);
}

TransientMode TouchInputControl::readTransientMode(const char* label)
{
	_tft->fillScreen(TFT_BLACK);
	DccTools::printFrame(_tft, label);
	drawGrid(MODEPADX, MODEPADY, MODEPADWIDTH, MODEPADHEIGHT, 1, MODEPADROWS);

	for(int i = 0; i < 4; i++) DccTools::drawString(_tft, modes[i+1], TFTW / 2, MODEPADY + 5 + (i * MODEPADHEIGHT), Font2, TFT_WHITE, TFT_RED, KEYPADTEXTSIZE, TextCenter);

	while (true)
	{
		if (!readTouchScreen(&_coordinates)) continue;
		if (_coordinates.X < MODEPADX || _coordinates.X > MODEPADX + MODEPADWIDTH) continue;
		if (_coordinates.Y < MODEPADY || _coordinates.Y > MODEPADY + (MODEPADHEIGHT * MODEPADROWS)) continue;

		delay(INPUTDELAY);
		return (TransientMode)(((_coordinates.Y - MODEPADY) / MODEPADHEIGHT)+1);
	}
}

void TouchInputControl::printKeypad(const char* label)
{
	_tft->fillScreen(TFT_BLACK);
	DccTools::printFrame(_tft, label);
	drawGrid(KEYPADX, KEYPADY, KEYPADWIDTH, KEYPADHEIGHT, KEYPADCOLUMNS, KEYPADROWS);

	_tft->setTextColor(TFT_YELLOW, TFT_RED);
	_tft->setTextSize(KEYPADTEXTSIZE);
	for (long i = 0; i < 9; i++)
		_tft->drawChar('0' + i + 1, KEYPADX + ((i%KEYPADCOLUMNS)*KEYPADWIDTH) + KEYPADDELTAX, KEYPADY + ((i / KEYPADCOLUMNS)*KEYPADHEIGHT) + KEYPADDELTAY, 4);
	int y = KEYPADY + ((KEYPADROWS - 1)*KEYPADHEIGHT) + KEYPADDELTAY;
	_tft->drawChar(DECIMALPOINTCHAR, KEYPADX + KEYPADDELTAX, y, 4);
	_tft->drawChar('0', KEYPADX + KEYPADWIDTH + KEYPADDELTAX, y, 4);
	_tft->drawString("Del", KEYPADX + KEYPADWIDTH + KEYPADWIDTH + KEYPADDELTAX, y, 4);

	for (int i = 0; i < MAXUSERVALUES; i++)
	{
		printUserValue(i);
		DccTools::drawBox(_tft, KEYPADFUNCTIONX + KEYPADWIDTH + (KEYPADWIDTH / 2), KEYPADFUNCTIONY + (i*KEYPADHEIGHT), KEYPADWIDTH / 2, KEYPADHEIGHT, TFT_WHITE, TFT_RED);
		_tft->drawChar('M', KEYPADFUNCTIONX + KEYPADWIDTH + (KEYPADWIDTH / 2) + 5, KEYPADFUNCTIONY + (i*KEYPADHEIGHT) + KEYPADDELTAY, 4);
	}
	DccTools::drawBox(_tft, KEYPADFUNCTIONX, KEYPADFUNCTIONY + (((KEYPADROWS - 1)*KEYPADHEIGHT)), KEYPADWIDTH*KEYPADFUNCTIONCOLUMNS, KEYPADHEIGHT, TFT_WHITE, TFT_RED);
	_tft->drawCentreString("Set", KEYPADFUNCTIONX + ((KEYPADWIDTH + (KEYPADWIDTH / 2)) / 2), KEYPADFUNCTIONY + (((KEYPADFUNCTIONROWS - 1)*KEYPADHEIGHT)) + KEYPADDELTAY, KEYPADFONT);
}

float TouchInputControl::readKeyPad(const char* title, const char* label, byte inputIntLength = INPUTINTLENGTH, byte inputFracLength = INPUTFRACLENGTH, int inputMaxValue = INPUTMAXVALUE)
{
	_inputIntLength = inputIntLength;
	_inputFracLength = inputFracLength;
	_inputMaxValue = inputMaxValue;
	_inputLength = _inputIntLength + _inputFracLength + (_inputFracLength == 0 ? 0 : 1);
	if (_inputLength > INPUTMAXLENGTH) _inputLength = INPUTMAXLENGTH;

	printKeypad(title);
	DccTools::drawString(_tft, label, 180, 50, Font4, TFT_WHITE, TFT_BLACK, 1, TextLeft);
	resetKeys();
	printInput(TFT_YELLOW);

	while (true)
	{
		if (!readTouchScreen(&_coordinates)) continue;

		int col = (_coordinates.X - KEYPADX) / KEYPADWIDTH;
		int row = (_coordinates.Y - KEYPADY) / KEYPADHEIGHT;
		if (row < 0 || row >= KEYPADROWS) continue;
		if (col < 0) continue;

		if (col < KEYPADCOLUMNS) handleKey(col, row);
		else if (row == KEYPADROWS - 1) return keysToFloat();
		else if (row < KEYPADROWS - 1 && DccTools::isIn(_coordinates.X, KEYPADFUNCTIONX, KEYPADFUNCTIONX + KEYPADWIDTH + (KEYPADWIDTH / 2))) return getUserValue(row);
		else if (row < KEYPADROWS - 1 && DccTools::isIn(_coordinates.X, KEYPADFUNCTIONX + KEYPADWIDTH + (KEYPADWIDTH / 2), KEYPADFUNCTIONX + (KEYPADWIDTH * 2)))
		{
			setUserValue(row, keysToFloat());
			printUserValue(row);
		}

		delay(INPUTDELAY);
	}
}

void TouchInputControl::printInput(uint16_t color)
{
	DccTools::drawBox(_tft, 90, 50, 80, 26, TFT_BLACK, TFT_BLACK);
	DccTools::drawString(_tft, _keys, 170, 50, Font4, color, TFT_BLACK, 1, TextRight);
	int fh = _tft->fontHeight(4);
	int fw = _tft->textWidth("0", 4);
	int fwd = _inputFracLength == 0 ? 0 : _tft->textWidth(".", 4);
	int x = 170 - (_inDecimals ? fw * (_inputFracLength - _cursor) : (fw * (_inputFracLength + 1 + _cursor)) + fwd);
	_tft->drawFastHLine(70, 50 + fh, 100, TFT_BLACK);
	_tft->drawFastHLine(x, 50 + fh, fw, color);
}

void TouchInputControl::resetKeys()
{
	DccTools::floatToStr(0, _inputLength, _inputFracLength, _keys);
	_cursor = 0;
	_inDecimals = false;
}

void TouchInputControl::handleKey(byte col, byte row)
{
	char key = KEYPADKEYS[row][col];
	switch (key)
	{
	case DECIMALPOINTCHAR: toggleInDecimals(); break;
	case DELETECHAR: deleteKey(); break;
	default: addKey(key); break;
	}
	printInput(TFT_YELLOW);
}

void TouchInputControl::toggleInDecimals()
{
	if (_inputFracLength == 0) return;
	_inDecimals = !_inDecimals;
	setCursor(0);
}

void TouchInputControl::setCursor(int cursor)
{
	if (cursor < 0) cursor = 0;
	else if (_inDecimals && cursor >= _inputFracLength) cursor = _inputFracLength - 1;
	else if (!_inDecimals && cursor >= _inputIntLength - 1) cursor = _inputIntLength - 1;
	_cursor = cursor;
}

void TouchInputControl::addKey(char key)
{
	char keys[INPUTMAXLENGTH+1];
	strcpy(keys, _keys);
	if (_inDecimals)
	{
		keys[_cursor + (_inputLength - _inputFracLength)] = key;
		setKeys(keys, _cursor + 1);
	}
	else
	{
		for (byte i = 1; i < _inputIntLength - _cursor; i++) keys[i - 1] = keys[i];
		keys[(_inputIntLength - _cursor) - 1] = key;
		setKeys(keys, _cursor);
	}
}

void TouchInputControl::deleteKey()
{
	char keys[INPUTMAXLENGTH + 1];
	strcpy(keys, _keys);
	if (_inDecimals)
	{
		for (int i = _cursor  + (_inputLength - _inputFracLength); i < _inputLength-1; i++) keys[i] = keys[i+1];
		keys[_inputLength -1] = '0';
		setKeys(keys, _cursor - 1);
	}
	else
	{
		for (int i = (_inputIntLength - _cursor) - 1; i > 0; i--) keys[i] = keys[i - 1];
		keys[0] = '0';
		setKeys(keys, _cursor);
	}
}

bool TouchInputControl::flashError()
{
	printInput(TFT_RED);
	delay(500);
	return false;
}

bool TouchInputControl::setKeys(const char* keys, int cursor)
{
	float f = DccTools::str2Float(keys);
	if (f < INPUTMINVALUE || f > _inputMaxValue) return flashError();
	DccTools::floatToStr(f, _inputLength, _inputFracLength, _keys);
	setCursor(cursor);
	return true;
}

float TouchInputControl::keysToFloat()
{
	return DccTools::str2Float(_keys);
}

float TouchInputControl::getUserValue(int idx)
{
	return _inputFracLength == 0 ? DccTools::getIntUserValue(idx) : DccTools::getFloatUserValue(idx);
}

void TouchInputControl::setUserValue(int idx, float value)
{
	_inputFracLength == 0 ? DccTools::setIntUserValue(idx, value) : DccTools::setFloatUserValue(idx, value);
}

