# Introduction #

The DcCalibrator is a reimplementation of the Arduino software for the [Scullcom DC Calibrator](https://www.youtube.com/watch?v=V9l5kc_o_dA&list=PLUMG8JNssPPzKeYkrDJpXV0J0Eu5myQqi) project.

It uses the [TFT_ILI9341 library from Bodmer](https://github.com/Bodmer/TFT_ILI9341) to speed up the tft.  
For that reason it needs the 1.2k/1.8k voltage divider modification on the TFT print to work correctly.
Also, you need to open the fonts used and define the pins for the touch in User_Setup.h of the TFT_ILI9341 library.
```

//#define LOAD_GLCD   // Font 1. Original Adafruit 8 pixel font needs ~1820 bytes in FLASH
#define LOAD_FONT2  // Font 2. Small 16 pixel high font, needs ~3534 bytes in FLASH, 96 characters
#define LOAD_FONT4  // Font 4. Medium 26 pixel high font, needs ~5848 bytes in FLASH, 96 characters
//#define LOAD_FONT6  // Font 6. Large 48 pixel font, needs ~2666 bytes in FLASH, only characters 1234567890:-.apm


#define TFT_CS   10 // Chip select control pin
#define TFT_DC   9  // Data Command control pin
#define TFT_RST  8  // Reset pin (could connect to Arduino RESET pin)

#define FAST_GLCD
#define FAST_LINE
#define SUPPORT_TRANSACTIONS
```

### Release Versions ###
* 1.0, 05.Jun.2018, first release
* 1.2, 13.Aug.2018, added remote control
* 1.3, 16.Oct.2018, added transient mode, added int user values for duration, small ui changes. Needs newest DccControl software.

Please report errors if you find some!

This readme and documentation is under construction...

You can find the corresponding remote control software for Windows [here](https://bitbucket.org/Rytikar/dcccontrol/src/master/)

### Installation ###

* Click on the [Downloads](https://bitbucket.org/Rytikar/dccalibrator/downloads/) link.
* Click on 'Download repository'
* You will receive a .zip file, containing the repository
* Unzip the file into a folder called 'dccalibrator'. That is important, as the Arduino IDE needs it to work
* You will end up with lots of files. That's because I have implemented the software with Visual Studio, that's where the .sln and other VS specific files come from.. But don't worry...
* Open the dcload.ino file in the Arduino IDE as usual.
* Build and upload the project to your Arduino as usual. 
* Have fun

### Documentation ###

The look and feel resembles the original software. Still, I've changed the gui a bit and added some features.

* Implements all of the features available in Louis original software. Some enhancements done.
* Voltage ON/OFF is a toggle pad now, not two single ones.
* The voltage range is 0 to 12 volts (at lest on my unit). The voltage will blink red shortly when entering values out of range.
* I've implemented user values for fast voltage selection. Use the M pad to store the current voltage in the specific user value.
* I'm trying to address the problem of users getting displays with changing touch configurations, I for sure got a different one than Louis.
For that reason there is a two second startup window where you can touch the screen any place and configure your setup. 
This needs to be done only once and is then stored in EEprom but can be repeated on next startup.
* The transient mode can be set up as Toggle, Loop, Raise or Fall. Define low, high and step voltages.
Define a duration in mS (max 9999). The 'ON' or 'OFF' text changes to 'ON(T)' or 'OFF (T)' to show the unit is running in transient mode.
* When setting duration in mS the usual three user values for voltages are replaced by three values for duration (0-9999) 
### Who do I talk to? ###

* dps@rosenkilde-hoppe.dk

### Thanks ###

To:

* [Scullcom](http://www.scullcom.uk/)
* [VisualMicro](https://www.visualmicro.com/)
* [Bodmer](http://www.instructables.com/id/Arduino-TFT-display-and-font-library/)
