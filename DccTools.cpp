#include <EEPROM.h>
#include <TFT_ILI9341.h>
#include "DccTools.h"
#include "DccConst.h"
#include "InputControl.h"

char _tmpStr[INPUTMAXLENGTH + 1];
float _volts;
TransientMode _transientMode = TransientOff;

const char* DccTools::inputToA(float input, int _inputFracLength = INPUTFRACLENGTH, int width = INPUTMAXLENGTH)
{
	return floatToStr(input, width, _inputFracLength, _tmpStr);
}

bool DccTools::isIn(int value, int minValue, int maxValue)
{
	return (value >= minValue) && (value <= maxValue);
}

bool DccTools::isIn(int x, int y, int minX, int maxX, int minY, int maxY)
{
	return (x >= minX) && (x <= maxX) && (y >= minY) && (y <= maxY);
}

float DccTools::readFloat(int address)
{
	byte bytes[4];
	for (int i = 0; i < 4; i++) bytes[i] = EEPROM.read(address + i);
	float value = *((float *)bytes);
	return isnan(value) ? 0 : value;
}

float DccTools::writeFloat(int address, float value)
{
	byte bytes[4];
	*((float *)bytes) = value;
	for (int i = 0; i < 4; i++) EEPROM.write(address + i, bytes[i]);
	return value;
}

int DccTools::readInt(int address)
{
	byte bytes[2];
	for (int i = 0; i < 2; i++) bytes[i] = EEPROM.read(address + i);
	int value = *((int *)bytes);
	return value < 0 ? 0 : value;
}

int DccTools::writeInt(int address, int value)
{
	byte bytes[2];
	*((int *)bytes) = value;
	for (int i = 0; i < 2; i++) EEPROM.write(address + i, bytes[i]);
	return value;
}

void DccTools::printFrame(TFT_ILI9341* tft, const char *title)
{
	char tmp[10] = "rrh";
	tft->fillScreen(TFT_BLACK);
	tft->drawRect(0, 0, TFTW, TFTH, TFT_MAGENTA);
	DccTools::drawString(tft, title, TFTHALFW, 10, Font4, TFT_WHITE, TFT_BLACK, 1);
	DccTools::drawString(tft, "Scullcom Hobby Electronics", TFTHALFW, 220, Font2, TFT_WHITE, TFT_BLACK);
	DccTools::drawString(tft, strcat(tmp, DCCVERSION), TFTW-2, 220, Font2, TFT_WHITE, TFT_BLACK, 1, TextRight);
}

void DccTools::drawBox(TFT_ILI9341* tft, int x, int y, int w, int h, uint16_t lineColor, uint16_t backgroundColor)
{
	for (int i = 0; i < h; i++) tft->drawFastHLine(x, y + i, w, backgroundColor);
	tft->drawRect(x, y, w, h, lineColor);
}

void DccTools::drawCrossHair(TFT_ILI9341* tft, int x, int y, uint16_t color)
{
	tft->drawRect(x - 10, y - 10, 20, 20, color);
	tft->drawFastHLine(x - 5, y, 10, color);
	tft->drawFastVLine(x, y - 5, 10, color);
}

uint16_t _lastFgColor = 1;
uint16_t _lastBgColor = 1;
uint8_t _lastTextSize = 255;

const char* DccTools::drawString(TFT_ILI9341* tft, const char* s, int x, int y, Font font, uint16_t fgColor, uint16_t bgColor, uint8_t textSize = 1, DrawPosition position = TextCenter)
{
	if (fgColor != _lastFgColor || bgColor != _lastBgColor)
	{
		tft->setTextColor(fgColor, bgColor);
		_lastFgColor = fgColor;
		_lastBgColor = bgColor;
	}
	if (textSize != _lastTextSize)
	{
		tft->setTextSize(textSize);
		_lastTextSize = textSize;
	}
	switch (position)
	{
		case TextCenter : tft->drawCentreString(s, x, y, font); break;
		case TextRight  : tft->drawRightString(s, x, y, font); break;
		default: tft->drawString(s, x, y, font); break;
	}

	return s;
}

char* DccTools::floatToStr(float value, int width, int precision, char* result, char filler = SPACECHAR)
{
	char str[14];               // Array to contain decimal string
	uint8_t ptr = 0;            // Initialise pointer for array
	int8_t  digits = 1;         // Count the digits to avoid array overflow
	float rounding = 0.5;       // Round up down delta

	if (precision > 7) precision = 7;         // Limit the size of decimal portion

	for (uint8_t i = 0; i < precision; ++i) rounding /= 10.0; // Adjust the rounding value

	if (value < -rounding)    // add sign, avoid adding - sign to 0.0!
	{
		str[ptr++] = '-'; // Negative number
		str[ptr] = 0; // Put a null in the array as a precaution
		digits = 0;   // Set digits to 0 to compensate so pointer value can be used later
		value = -value; // Make positive
	}

	value += rounding; // Round up or down

	if (value >= 2147483647) return NULL;

	// No chance of overflow from here on

	// Get integer part
	unsigned long temp = (unsigned long)value;

	// Put integer part into array
	ltoa(temp, str + ptr, 10);
	uint8_t i = 0;

	if (precision > 0)
	{
		// Find out where the null is to get the digit count loaded
		while ((uint8_t)str[ptr] != 0) ptr++; // Move the pointer along
		digits += ptr;                  // Count the digits

		str[ptr++] = '.'; // Add decimal point
		str[ptr] = '0';   // Add a dummy zero
		str[ptr + 1] = 0; // Add a null but don't increment pointer so it can be overwritten

		value -= temp; // Get the decimal portion

		// Get decimal digits one by one and put in array
		// Limit digit count so we don't get a false sense of resolution
		while ((i < precision) && (digits < 9)) // while (i < precision) for no limit but array size must be increased
		{
			i++;
			value *= 10;       // for the next decimal
			temp = value;      // get the decimal
			ltoa(temp, str + ptr, 10);
			ptr++; digits++;         // Increment pointer and digits count
			value -= temp;     // Remove that digit
		}
	}

	if (width < 0) width = strlen(str);
	for(i = 0; i < width - strlen(str); i++) result[i] = filler;
	result[i] = 0;

	return strcat(result, str);
}

float DccTools::str2Float(const char * str)
{
	unsigned char abc;
	float ret = 0, fac = 1;
	for (abc = 9; abc & 1; str++) 
	{
		abc = *str == '-' ?
			(abc & 6 ? abc & 14 : (abc & 47) | 36)
			: *str == '+' ?
			(abc & 6 ? abc & 14 : (abc & 15) | 4)
			: *str > 47 && *str < 58 ?
			abc | 18
			: (abc & 8) && *str == '.' ?
			(abc & 39) | 2
			: !(abc & 2) && (*str == ' ' || *str == '\t') ?
			(abc & 47) | 1
			:
			abc & 46;
		if (abc & 16) ret = abc & 8 ? *str - 48 + ret * 10 : (*str - 48) / (fac *= 10) + ret;
	}
	return abc & 32 ? -ret : ret;
}

float DccTools::getVolts()
{
	return _volts;
}

float DccTools::setVolts(MCP4922* dac, float volts)
{
	if (volts >= 8.001)     DccTools::setDac(dac, HIGH, HIGH, HIGH, (volts / 4.0) * 1000.0);
	else if (volts > 4.000) DccTools::setDac(dac, HIGH, LOW, HIGH, (volts / 2.0) * 1000.0);
	else if (volts > 2.000) DccTools::setDac(dac, HIGH, LOW, LOW, volts * 1000.0);
	else if (volts > 1.000) DccTools::setDac(dac, LOW, LOW, HIGH, volts * 2000.0);
	else                    DccTools::setDac(dac, LOW, LOW, LOW, volts * 4000.0);
	_volts = volts;
	return volts;
}

bool DccTools::isTransient()
{
	return _transientMode != TransientOff;
}

TransientMode DccTools::getTransientMode()
{
	return _transientMode;
}

void DccTools::setTransientMode(TransientMode transientMode)
{
	_transientMode = transientMode;
}

void DccTools::setDac(MCP4922* dac, byte a4, byte a1, byte a3, int volts)
{
	digitalWrite(A4, a4);                        //set reference voltage
	digitalWrite(A1, a1);                        //set INA105 gain switch
	digitalWrite(A3, a3);                        //set INA105 gain switch
	SPI.begin();                                 //Initializes the SPI bus
	dac->Set(volts, 0);                          //write voltage to DAC
	SPI.end();                                   //Disables the SPI bus
}

bool DccTools::getOutputState()
{
	return digitalRead(A2) == HIGH;
}

bool DccTools::setOutputState(bool isOn)
{
	digitalWrite(A2, isOn ? HIGH : LOW);
	return isOn;
}

float DccTools::getFloatUserValue(int idx)
{
	if (idx < 0 || idx >= MAXUSERVALUES) idx = 0;
	return readFloat(USERVALUESFLOATEEADDRESS + (idx * 4));
}

float DccTools::setFloatUserValue(int idx, float value)
{
	if (idx < 0 || idx >= MAXUSERVALUES) idx = 0;
	writeFloat(USERVALUESFLOATEEADDRESS + (idx * 4), value);
	return value;
}

int DccTools::getIntUserValue(int idx)
{
	if (idx < 0 || idx >= MAXUSERVALUES) idx = 0;
	return readInt(USERVALUESINTEEADDRESS + (idx * 2));
}

int DccTools::setIntUserValue(int idx, int value)
{
	if (idx < 0 || idx >= MAXUSERVALUES) idx = 0;
	writeInt(USERVALUESINTEEADDRESS + (idx * 2), value);
	return value;
}

char* DccTools::parseArgument(const char* src, char* dst, int* idx)
{
	int tmpIdx = 0;
	while (src[*idx] && src[*idx] != ARGUMENTDELIMITER) dst[tmpIdx++] = src[(*idx)++];
	if (src[*idx] == ARGUMENTDELIMITER) (*idx)++;
	dst[tmpIdx] = NULCHAR;
	return dst;
}

