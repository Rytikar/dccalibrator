#pragma once

static const char DCCVERSION[] = "1.3";

const int MAXVOLTS = 12;

const byte TOUCH_SCK = 3;
const byte TOUCH_CS = 4;
const byte TOUCH_MOSI = 5;
const byte TOUCH_MISO = 6;
const byte TOUCH_IRQ = 7;
const byte DAC_CS = 2;
const byte DAC_MOSI = 11;
const byte DAC_SCK = 13;
const byte DAC_LDAC = 14;

const int KEYPADX = 5;
const int KEYPADY = 85;
const int KEYPADCOLUMNS = 3;
const int KEYPADROWS = 4;
const int KEYPADWIDTH = 60;
const int KEYPADHEIGHT = 30;
const int KEYPADDELTAX = 10;
const int KEYPADDELTAY = 3;
const int KEYPADTEXTSIZE = 1;

const int KEYPADFUNCTIONX = 10 + (KEYPADCOLUMNS*KEYPADWIDTH) + 5;
const int KEYPADFUNCTIONY = 85;
const int KEYPADFUNCTIONWIDTH = 90;
const int KEYPADFUNCTIONCOLUMNS = 2;
const int KEYPADFUNCTIONROWS = 4;

const int MODEPADX = 110;
const int MODEPADY = 60;
const int MODEPADWIDTH = 100;
const int MODEPADHEIGHT = 30;
const int MODEPADROWS = 4;

const int VOLTSY = 60;

const int SETVOLTX = 20;
const int SETVOLTY = 125;
const int SETVOLTW = 100;
const int SETVOLTH = 40;
const int SETTRANSIENTX = 20;
const int SETTRANSIENTY = 175;
const int SETTRANSIENTW = 100;
const int SETTRANSIENTH = 40;
const int TOGGLEVOLTX = 200;
const int TOGGLEVOLTY = 150;
const int TOGGLEVOLTW = 100;
const int TOGGLEVOLTH = 40;

const char NULCHAR = '\0';
const char SPACECHAR = ' ';
const char DECIMALPOINTCHAR = '.';
const char DELETECHAR = 'D';

const int USERVALUESFLOATEEADDRESS = 0x0;
const int USERVALUESINTEEADDRESS = 0x20;
const int CALIBRATIONEEADDRESS = 0x10;
const int MAXUSERVALUES = 3;

const int INPUTMAXLENGTH = 6;		// 00.000
const int INPUTMINVALUE = 0;
const int INPUTFRACLENGTH = 3;
const int INPUTINTLENGTH = 2;
const int INPUTMAXVALUE = 12;
const int INPUTDELAY = 500;                 //key bounce delay - was 1000 (change value as required)

const int TFTW = 320;
const int TFTH = 240;
const int TFTHALFW = TFTW / 2;
const int TFTHALFH = TFTH / 2;
