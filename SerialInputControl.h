#pragma once

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "InputControl.h"

class SerialInputControl : public InputControl
{
private:
	unsigned long _heartbeatMillis = 0;
	bool _isConnected = false;
	bool resetCommand();
	bool pendingCommand();
	void resetTimeout();
	bool isTimedOut();

	bool readCommand(char* command);
	void sendStatus();
	void sendMemory();
	void sendFirmware();
	byte sendFloat(float value, bool sendDelimiter, byte checkSum);
	byte sendInt(int value, bool sendDelimiter, byte checkSum);
	byte sendBool(bool value, bool sendDelimiter, byte checkSum);
	byte sendString(const char* s, bool sendDelimiter, byte checkSum);
	byte sendChar(char c, byte checksum = 0);
	void finishSend(byte checkSum);
	byte checksum(const char* command);
	int serialRead();
	void handleStoreRequest();
	void handleTransientRequest(handleRequestCallback handleRequest);
	float parseFloat(const char* s, int* idx);
	int parseInt(const char* s, int* idx);
protected:

 public:
	 SerialInputControl();
	 bool checkRemote();
	 void checkInput(handleRequestCallback handleRequest) override;
};

